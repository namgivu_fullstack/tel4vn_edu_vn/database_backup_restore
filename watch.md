--- watch container
watch "docker ps --format '{{.ID}} {{.Names}} {{.Image}} {{.Networks}} {{.Ports}}' | column -t"

--- watch volume
watch docker volume ls 
