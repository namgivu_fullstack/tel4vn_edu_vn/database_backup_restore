CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  name TEXT,
  email TEXT
);


INSERT INTO users (name,          email)
VALUES
                  ('John Doe',    'johndoe@example.com'),
                  ('Jane Doe',    'janedoe@example.com'),
                  ('Peter Jones', 'peterjones@example.com')
;
